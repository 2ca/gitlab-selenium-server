const Promise = require('bluebird');
const path = require('path').posix;
const fs = require('fs-extra');
const emptyDir = Promise.promisify(fs.emptyDir);
const glob = Promise.promisify(require('glob'));
const webdriver = require('selenium-webdriver');
const chromedriver = require('chromedriver');
const request = Promise.promisify(require('request'));

const ChildExecutor = require('../util/child-executor');

// Environment variables
const SELENIUM_REMOTE_URL = process.env['SELENIUM_REMOTE_URL'] || 'http://localhost:4545/wd/hub';
const GITLAB_TARGET_SELENIUM_REMOTE_URL = process.env['GITLAB_TARGET_SELENIUM_REMOTE_URL'] || 'http://localhost:4444/wd/hub';
const GITLAB_SELENIUM_PROXY_LOG_DIR = process.env['GITLAB_SELENIUM_PROXY_LOG_DIR'] ? path.resolve(__dirname, '../../', process.env['GITLAB_SELENIUM_PROXY_LOG_DIR']) : path.resolve(__dirname, '../../test-logs/');

// Test timeouts
const ORIGINAL_TIMEOUT_INTERVAL = jasmine.DEFAULT_TIMEOUT_INTERVAL;
const NEW_TIMEOUT_INTERVAL = 8000;
const REQUEST_TIMEOUT_INTERVAL = 3000;

function waitForServer(url, timeout) {
  let hasTimedOut = false;

  if (timeout >= 0) {
    setTimeout(() => {
      hasTimedOut = true;
    }, timeout);
  }

  return (function waitRecursive() {
      return request({
        uri: url
      })
        .catch((err) => {
          if(!hasTimedOut) {
            return waitRecursive(url);
          } else {
            throw new Error(`Unable to connect to server ${url}, ${err}, ${err.stack}`);
          }
        });
  })();
}

describe('GitLab Selenium Server', () => {
  let gitlabSeleniumServerExecutor;
  let driver;

  beforeEach(async () => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = NEW_TIMEOUT_INTERVAL;

    console.log("Starting selenium server");
    // Start the target Selenium server
    await chromedriver.start([
      '--url-base=wd/hub',
      `--port=4444`,
      '--verbose',
      '--no-sandbox'
    ]);

    // We clear things out in the beforeEach so if something fails, we can reference the server output
    await emptyDir(GITLAB_SELENIUM_PROXY_LOG_DIR);
    // Wait for the target Selenium server to startup
    await waitForServer('http://localhost:4444/wd/hub/status', REQUEST_TIMEOUT_INTERVAL);

      // Start the proxy
      gitlabSeleniumServerExecutor = new ChildExecutor();
      const npmCommand = /^win/.test(process.platform) ? 'npm.cmd' : 'npm';
      gitlabSeleniumServerExecutor.exec(`${npmCommand} start`, {
        env: Object.assign({}, process.env, {
          'GITLAB_SELENIUM_PROXY_LOG_DIR': GITLAB_SELENIUM_PROXY_LOG_DIR,
          'GITLAB_TARGET_SELENIUM_REMOTE_URL': GITLAB_TARGET_SELENIUM_REMOTE_URL
        })
      })
        .catch((resultInfo) => {
          console.error(resultInfo.error);
          throw resultInfo.error;
        });

      // Wait for our shelled out proxy to startup
      await waitForServer(SELENIUM_REMOTE_URL, REQUEST_TIMEOUT_INTERVAL);

      const chromeCapabilities = webdriver.Capabilities.chrome();
      chromeCapabilities.set('chromeOptions', { args: ['--headless', '--no-sandbox', '--disable-gpu', '--disable-dev-shm-usage'] });

      driver = await new webdriver.Builder()
        .forBrowser('chrome')
        .withCapabilities(chromeCapabilities)
        // Automatically pulled from enviornment variable `SELENIUM_REMOTE_URL`
        // This should point at our GitLab Selenium server
        .usingServer(SELENIUM_REMOTE_URL)
        .build();
  });

  afterEach(async () => {
    if(gitlabSeleniumServerExecutor) {
      if (gitlabSeleniumServerExecutor.stderr.length > 0 ) {
        console.log('gitlabSeleniumServerExecutor stdout', gitlabSeleniumServerExecutor.stdout);
        console.log('gitlabSeleniumServerExecutor stderr', gitlabSeleniumServerExecutor.stderr);
      }

      gitlabSeleniumServerExecutor.child.kill();
    }

    chromedriver.stop();

    jasmine.DEFAULT_TIMEOUT_INTERVAL = ORIGINAL_TIMEOUT_INTERVAL;
  });

  it('Takes screenshot on key stroke', async () => {
    await driver.get('http://google.com/')

    const searchInput = driver.findElement(webdriver.By.name('q'))
    searchInput.sendKeys('webdriver')
    searchInput.sendKeys(webdriver.Key.ENTER);

    await driver.wait(webdriver.until.titleIs('webdriver - Google Search'), 1000);

    // Quit the test here so `selenium-logs.json` is generated and we can test for it below
    // Otherwise, we could do this in `afterEach`
    await driver.quit();

    // Assert screenshots
    const screenshotFiles = await glob(path.join(GITLAB_SELENIUM_PROXY_LOG_DIR, './+([0-9a-f])/screenshots/*.png'));
    expect(screenshotFiles.length).toEqual(3);

    // Assert JSON log file
    const logFiles = await glob(path.join(GITLAB_SELENIUM_PROXY_LOG_DIR, './+([0-9a-f])/selenium-logs.json'));
    expect(logFiles.length).toEqual(1);
  });
});
