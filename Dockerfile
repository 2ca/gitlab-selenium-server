# Image stack:
# selenium/standalone-chrome ->
# selenium/node-chrome -> https://github.com/SeleniumHQ/docker-selenium/blob/3fb76ba36cfa241a7927d432fc162553063259c3/StandaloneChrome/Dockerfile#L5
# selenium/node-base -> https://github.com/SeleniumHQ/docker-selenium/blob/3fb76ba36cfa241a7927d432fc162553063259c3/NodeChrome/Dockerfile#L5
# selenium/base -> https://github.com/SeleniumHQ/docker-selenium/blob/3fb76ba36cfa241a7927d432fc162553063259c3/NodeBase/Dockerfile#L5
# ubuntu:16.04 -> https://github.com/SeleniumHQ/docker-selenium/blob/b5262f1e70cc1927cd31622b668e67628adf4777/Base/Dockerfile#L1
FROM selenium/standalone-chrome:4.1.0-20211123

USER root

WORKDIR /app

ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 10.19.0

# Install nvm
RUN wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.4/install.sh | bash

# Install Node and npm
RUN . $NVM_DIR/nvm.sh \
  && nvm install $NODE_VERSION \
  && nvm alias default $NODE_VERSION \
  && nvm use default

# Add Node and npm to path so the commands are available
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

# Confirm installation
RUN node -v
RUN npm -v

# Install app dependencies
COPY package.json package-lock.json ./
RUN npm install

# Bundle app source
COPY . .

# We don't expose port 4444 so people don't get confused if the accidentally
# try to use the backing Selenium server
EXPOSE 4545

HEALTHCHECK --interval=2s --timeout=5s --retries=3 --start-period=20s \
  CMD npm run health-check

# We have to combine these because there can only be one CMD
# and it is overridden from the base images.
#
# Start the Selenium server
# This is copied over https://github.com/SeleniumHQ/docker-selenium/blob/20995d207b1e8d1eef09d4f3eba30cb7ec65334f/StandaloneChrome/Dockerfile#L13
# And is normally run as a CMD here, https://github.com/SeleniumHQ/docker-selenium/blob/20995d207b1e8d1eef09d4f3eba30cb7ec65334f/NodeBase/Dockerfile#L117
#
# And then start the app
CMD ["/bin/bash", "-c", "/opt/bin/entry_point.sh & npm start"]
