function createSeleniumError(err) {
  return {
    state: 'unknown error',
    sessionId: null,
    hCode: null,
    value: {
      localizedMessage: err.message,
      cause: null,
      stackTrace: err.stack.split('\n'),
      suppressed: [],
      message: err.message,
      hCode: null,
      class: 'gitlab.selenium.proxy.SomethingWentWrong'
    },
    class: 'org.openqa.selenium.remote.Response',
    status: 13
  };
}

module.exports = createSeleniumError;
