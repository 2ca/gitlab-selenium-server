function isSessionReq(req) {
  return (/session\/[a-f\d-]+/i).test(req.path);
}

module.exports = isSessionReq;
